$(document).ready(function () {

    checkSearch();
    checkHeight();

    $.cookie.defaults = ({path: '/', expires: 30});
// Get our cookies
    var cookies = {
        left_panel: $.cookie('left_panel'),
        right_panel: $.cookie('right_panel')
    };

// Checking cookies
    for (var key in cookies) {
        if (cookies[key] == 1) {
            var cookie_marker = 1;
            mini_panel(key, cookie_marker);
        } else {
        }
    }

// Add new cookie with panel status (1 - hide)
    function add_cookie(element) {
        $.cookie(element, '1');
    }


// Mini-panel (when we hide panel we get mini-panel)
    function mini_panel(panel, cookie_marker) {

        if (cookie_marker === 1 && key == 'left_panel') {
            $('.' + panel).css('left', '0');
            $('section').css('margin-left', '40px');
            $('.' + panel).addClass('mini_panel');
        } else if (cookie_marker === 1 && key == 'right_panel') {
            $('.' + panel).css('right', '0');
            $('section').css('margin-right', '40px');
            $('.' + panel).addClass('mini_panel');

        } else if (panel === 'left_panel') {
            $('.' + panel).stop().animate({left: '-100px'}, 200, function () {
                $('.' + panel).addClass('mini_panel');
                add_cookie(panel);
            });
        } else if (panel === 'right_panel') {
            $('.' + panel).stop().animate({right: '-100px'}, 200, function () {
                $('.' + panel).addClass('mini_panel');
                add_cookie(panel);
            });
        }
    }

// Restore panel from mini to normal
    function restore_panel(panel) {

        if (panel == 'left_panel') {
            $('.' + panel).stop().animate({'left': '-30px'}, 200, function () {
                $('.filter_panel').css('left', '-100px');
                $('.' + panel).removeClass('mini_panel');
                $('.filter_panel').stop().animate({'left': '0px'}, 200);
                $('section').stop().animate({'margin-left': '110px'}, 200);
            });
        } else {
            $('.' + panel).stop().animate({'right': '-30px'}, 200, function () {
                $('.right_panel').css('right', '-100px');
                $('.' + panel).removeClass('mini_panel');
                $('.right_panel').stop().animate({'right': '0px'}, 200);
                $('section').stop().animate({'margin-right': '110px'}, 200);
            });
        }
        $.cookie(panel, null);
    }

// Hide our panels with simple jQuery animation
    function hide_panel(panel) {
        if (panel == 'left_panel') {
            $('.' + panel).stop().animate({left: '0px'}, 200, mini_panel(panel));
            $('section').stop().animate({'margin-left': '40px'}, 200);
        } else {
            $('.' + panel).stop().animate({right: '0px'}, 200, mini_panel(panel));
            $('section').stop().animate({'margin-right': '40px'}, 200);
        }
    }

// Status our navigation panels, tech function for keyboard shortcuts
    function panels_status(panel) {
        if ($.cookie(panel) == 1) {
            restore_panel(panel);
        } else {
            hide_panel(panel);
        }
    }


// Click event on panels (minimization panel)
    $('.hide_panel').on('click', function () {
        var panel = $(this).parent().attr('class');
        hide_panel(panel);
    });

// Click event for restoring panel
    $('.maxi_panel').on('click', function () {
        var panel = $(this).parent().attr('class').split(' ')[0]; // take only first class
        restore_panel(panel);
    });

// Scroll event for categories in both panels (lections, books, matherials, my, etc)
//    $(window).on('scroll', function () {
//        var y_scroll = $(window).scrollTop();
//        if (y_scroll > 60) {
//            $('nav > div').stop().animate({'position': 'fixed'}, 100);
//        } else {
//            $('nav > div').stop().animate({'padding-top': '100px'}, 100);
//        }
//    });

//File courses field
    $('#Files_courses').on('change', function (e) {
        if (typeof e.added != 'undefined') {
            var data = ([
                e.added.programs.join(),
                e.added.faculties.join(),
                e.added.term,
                e.added.year + " рік",
                e.added.level
            ]);
            var tagField = $('#Files_tags');
            (tagField.val() != []) ?
                tagField.val($('#Files_tags').val() + "," + data).trigger('change') :
                tagField.val(data).trigger('change');
        }
        ;
    });

//    $('.search_input').on('keyup', function () {
//        var query = $(this).val();
//        if (query.length >= 2) {
//            $.ajax({
//                url: "http://" + window.location.host + "/files/search",
//                data: {'q': query}
//            }).done(function (data) {
//                    console.log(data);
//                });
//        }
//    });

    var movies = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/files/search?q=%QUERY',
            filter: function (movies) {
                return $.map(movies, function (movie) {
                    return {
                        value: movie.title
                    };
                });
            }
        }
    });

// Initialize the Bloodhound suggestion engine
    movies.initialize();

// Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        displayKey: 'value',
        source: movies.ttAdapter()
    });

    $(document).load($(window).bind("resize", checkSearch));

    function checkSearch() {
        if (window.matchMedia('(min-width: 767px) and (max-width: 991px)').matches) {
            if ($('.search').css('display') != 'block') {
                $('#wrapper').removeClass("form-wrapper");
                $('.navbar-form').css('display', 'block');
                $('#wrapper').removeClass("slideup");
            }else{
                if (!$('#wrapper').hasClass("form-wrapper")) {
                    $('#wrapper').addClass("form-wrapper");
                }
            }
        }else{
            if ($('#wrapper').hasClass("form-wrapper")) {
                $('#wrapper').removeClass("form-wrapper");
            }
        }
    }

    function checkHeight(){
        if (($(window).height()-$(".content").height()-120) > ($("footer").height()+20)) {
            $('footer').css('position', 'absolute');
        };
    }

    $(".search").on( "click", function() {
        if($('#wrapper').hasClass("slideup")){
            $('.search').parent().addClass("active");
            $('#wrapper').removeClass("slideup").addClass("slidedown");
        }else{
            $('.search').parent().removeClass("active");
            $('#wrapper').removeClass("slidedown").addClass("slideup");
        }
    });

});
