<?php

date_default_timezone_set('UTC');
$yii = dirname(__FILE__) . '/../protected/lib/yiisoft/yii/framework/yii.php';
require_once dirname(__FILE__) . '/../protected/config/main.php';
require_once($yii);
$app = Yii::createWebApplication($config);
require dirname(__FILE__) . '/../protected/global.php';
$app->run();

/** Configuration for client scripts. Did not find better place to put it */
$cs = Yii::app()->getClientScript();
$cs->defaultScriptPosition = CClientScript::POS_END;
$cs->defaultScriptFilePosition = CClientScript::POS_END;
$cs->coreScriptPosition = CClientScript::POS_END;