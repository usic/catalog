<?php

class FileStorage extends EMongoFile
{
	public $name;
	public $file;
	public $downloads;
	public $status;

	public function collectionPrefix()
	{
		return 'storage';
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className class name
	 *
	 * @return  FileStorage static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('file', 'FileMimeToSize', 'defaultSize' => 5 * 1024 * 1024, 'on' => 'checkFile', 'mimeTypes' =>
				array('application/msword'            => 20 * 1024 * 1024, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 20 * 1024 * 1024,
				      'application/vnd.ms-excel'      => 20 * 1024 * 1024, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 20 * 1024 * 1024,
				      'application/vnd.ms-powerpoint' => 20 * 1024 * 1024, 'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 20 * 1024 * 1024,
				      'application/rtf'               => 20 * 1024 * 1024, 'application/pdf' => 100 * 1024 * 1024, 'image/vnd.djvu' => 100 * 1024 * 1024,
					'image/jpeg', 'image/png', 'text/plain', 'text/xml',
				      'application/zip'               => 20 * 1024 * 1024)
			),
			array('file', 'uniqueFile', 'on' => 'checkFile, childFile'),
			array('parent', 'match', 'pattern' => '/^[0-9a-fA-F]{24}$/', 'message' => 'Not MongoId',  'on' => 'childFile'),
			array('parent', 'checkParent', 'skipOnError' => true, 'on' => 'childFile'),
			array('user', 'default', 'value' => Yii::app()->user->login, 'on' => 'insert'),
			array('downloads', 'default', 'value' => 0, 'on' => 'insert'),
			array('status', 'default', 'value' => Files::STATUS_PENDING, 'on' => 'insert'),
			array('name, downloads, status, user, mime', 'safe'),
			array('name', 'required', 'on' => 'update,insert'),
		);
	}

	public function uniqueFile($attribute)
	{
		if ($this->count(array(
			'md5'  => md5_file($this->$attribute->getTempName()),
			'user' => Yii::app()->user->login))
		) {
			$this->addError($attribute, Yii::t('app', 'You have already uploaded this file'));
			return NULL;
		}
		if ($this->count(array(
			'md5'    => md5_file($this->$attribute->getTempName()),
			'status' => array('$ne' => Files::STATUS_NOT_APPROVED)))
		) {
			$this->addError($attribute, Yii::t('app', 'Such file already exist in our database'));
		}
	}

	public function checkParent($attribute, $params)
	{
		if (!$this->exists(array(
			'_id' => new MongoId($this->parent),
		))) {
			$this->addError($attribute, Yii::t('app', 'Parent file does not exist'));
		} else if ($this->exists(array(
			'parent' => $this->parent,
			'mime' => $this->file->getType(),
			'status' => array('$ne' => Files::STATUS_NOT_APPROVED)
		))){
			$this->addError($attribute, Yii::t('app', 'File with such parent and mime already exist in our database'));
		}
	}

	public function addFileToQueue()
	{
		$this->validate();

		$client = Yii::app()->yiinstalk->getClient('storage');
		$client
			->useTube("files")
			->put(CJSON::encode($this->attributes));
	}
}
