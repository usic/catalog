<?php

Class Statistics extends EMongoDocument
{
	public $agent;
	public $url;
	public $time;
	public $user;
	public $referrer;
	public $ip;
	public $params;
	public $post;
	public $ajax;
	public $memory;
	public $executionTime;
	public $code;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('agent, url, time, user, referrer, ip, params, post, ajax, code', 'safe', 'on' => 'insert'),
			array('agent', 'default', 'value' => Yii::app()->request->getUserAgent()),
			array('ajax', 'default', 'value' => Yii::app()->request->isAjaxRequest),
			array('post', 'default', 'value' => Yii::app()->request->isPostRequest),
			array('ip', 'default', 'value' => Yii::app()->request->userHostAddress),
			array('time', 'default', 'value' => new MongoTimestamp()),
			array('referrer', 'default', 'value' => Yii::app()->request->getUrlReferrer()),
			array('params', 'default', 'value' => $_REQUEST),
			array('url', 'default', 'value' => Yii::app()->request->requestUri),
			array('user', 'default', 'value' => (!Yii::app()->user->isGuest) ? Yii::app()->user->login : Yii::app()->user->name),
			array('memory', 'default', 'value' => round(memory_get_peak_usage() / (1024 * 1024), 2)),
			array('executionTime', 'default', 'value' => sprintf('%0.5f', Yii::getLogger()->getExecutionTime())),
		);
	}

	public function beforeValidate()
	{
		$error = $this->code->sender->errorHandler;
		$this->code = is_array($error->getError()) ? $error->getError()['code'] : 200;
		unset($this->event);
		return parent::beforeValidate();
	}
}