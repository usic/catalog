<?php
class FileVote extends EMongoEmbeddedDocument
{
    public $user;
    public $created;
    public $vote;

    public function rules()
    {
    return array(
        array('user,vote,created', 'required'),
        array('vote','match','pattern'=>'/^-?(1|2)$/'),
        );
    }
    public function beforeValidate()
    {
        $this->created = new MongoDate();
    }
}
