<?php


class Comment extends CFormModel{

	public $comment;
	public $file_id;

	public $user;
	public $time_created;

	private $_file;

	public function rules()
	{
		return array(
			array('comment, file_id', 'required', 'on' => 'addComment'),
			array('comment', 'length', 'max' => 500, 'on' => 'addComment'),
			array('file_id', 'match', 'pattern' => '/^[0-9a-fA-F]{24}$/', 'message' => 'Not MongoId'),
			array('file_id', 'fileApproved', 'skipOnError' => true),
			array('user', 'default', 'value' => Yii::app()->user->login, 'on' => 'addComment'),
			array('time_created', 'default', 'value' => new MongoDate(), 'on' => 'addComment'),
		);
	}

	public function fileApproved($attribute, $params)
	{
		$this->_loadFile();
		if(is_null($this->_file)){
			$this->addError('file_id', 'No such file');
		} else if(!$this->_file->approved()){
			$this->addError('file_id', 'File is not approved');
		}
	}

	public function saveComment()
	{
		$this->_file->comments[] = array('user' => $this->user, 'time_created' => $this->time_created, 'comment' => $this->comment);
		return $this->_file->save(false);
	}

	public function getFileComments()
	{
		return $this->_file->comments;
	}

	private function _loadFile()
	{
		if(!isset($this->_file)){
			$this->_file = Files::model()->findBy_id($this->file_id);
		}
	}
} 