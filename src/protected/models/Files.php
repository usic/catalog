<?php

class Files extends EMongoDocument
{
	public $name;
	/* @string file name */
	public $category;
	/* @string document category */
	public $user;
	/* @string user uploaded document */

	public $status;
	/* @int document status */
	public $title;
	/* @string material description */
	public $authors;
	/* @string authors of document */
	public $courses;
	/* @string courses which includes document */
	public $description;
	/* @text material description in text */
	public $description_mkd;
	/* @text material description in markdown */
	public $year;
	/* @int material version */
	public $time_approved;
	/* @mongodate approved material */
	public $time_updated;
	/* @mongodate updated material */

	public $rating;
	/* @float calculating every hour */

	public $votes;

	public $comments;

	public $parent;

	public $coursesData;

	public $sphinx_id;

	/* CONSTANTS */
	const
		TYPE_LECTURES = 'lectures',
		TYPE_BOOKS = 'books',
		TYPE_TEXTBOOKS = 'textbooks',
		TYPE_MATERIALS = 'materials',
		STATUS_APPROVED = 1,
		STATUS_NOT_APPROVED = 2,
		STATUS_PENDING = 3,
		STATUS_CHILD_FILE = 4;

	// This has to be defined in every model, this is same as with standard Yii ActiveRecord
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function collectionName()
	{
		return 'storage.files';
	}

	public function versioned()
	{
		return true;
	}

	public function versionField()
	{
		return '_v'; // This is actually the default value in EMongoDocument
	}

	public function rules()
	{
		return array(
			array('title, year,description_mkd, courses, description, category, authors', 'required', 'on' => 'update'),
			array('authors', 'length', 'max' => 140),
			array('category', 'in', 'range' => array_keys(CListDataHelper::getListData('category'))),
			array('status', 'in', 'range' => array_keys(CListDataHelper::getListData('status'))),
			array('year', 'numerical', 'min' => '1890', 'max' => date('Y'), 'integerOnly' => true),
			array('name, user', 'safe', 'on' => 'insert'),
			array('time_created, time_updated', 'safe'),
			array('description', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify'))
		);
	}

	public function attributeLabels()
	{
		return [
			'description_mkd' => Yii::t('form', 'Description'),
			'authors' => Yii::t('form', 'Authors'),
			'category' => Yii::t('form', 'Category'),
			'title' => Yii::t('form', 'Title'),
			'year' => Yii::t('form', 'Year')
		];
	}

	public function beforeSave()
	{
		if ($this->scenario == 'update') {
			$courseList = explode(',', $this->courses);
			foreach ($courseList as $course) {
				//TODO: add check on error
				$info = CJSON::decode(file_get_contents(sprintf('http://api.usic.at/api/v1/courses/autocomplete?q=%s&limit=100&match=1', $course)));
				foreach ($info as $value) {
					$this->coursesData[] = $value;
				}
			}
		}
		if ($this->getScenario() == 'update' && $this->status != self::STATUS_APPROVED) {
			$preview = new PreviewGenerator();
			$params = param('preview');
			$preview->previewPath = Yii::getPathOfAlias($params['path']) . DIRECTORY_SEPARATOR;
			foreach ($params['dimensions'] as $dim) {
				$preview->width = $dim['width'];
				$preview->height = $dim['height'];
				$preview->prefix = $dim['prefix'];
				$preview->generateById($this->_id);
			}
			$this->status = self::STATUS_PENDING;
		}
		$this->time_updated = new MongoDate();
		return parent::beforeSave();
	}

	public function beforeValidate()
	{
		if (!empty($this->description_mkd)) {
			$md = new CMarkdown;
			$pu = new CHtmlPurifier;
			$this->description_mkd = $pu->purify($this->description_mkd);
			$this->description = $md->transform($this->description_mkd);
		}
		return parent::beforeValidate();
	}

	public function file() // file storage
	{
		return FileStorage::model()->findOne(array('_id' => new MongoId($this->_id)));
	}

	public function recently($limit = 10)
	{
		$this->mergeDbCriteria([
			'order' => 'uploadDate DESC',
			'limit' => $limit,
		]);
		return $this;
	}

	public function approved()
	{
		$this->mergeDbCriteria([
			'condition' => array('status' => Files::STATUS_APPROVED),
		]);
		return $this;
	}

	public function notApproved()
	{
		$this->mergeDbCriteria([
			'condition' => ['status' => Files::STATUS_NOT_APPROVED],
			'sort'      => ['time_updated' => -1]
		]);
		return $this;
	}

	public function pending()
	{
		$this->mergeDbCriteria([
			'condition' => array('status' => Files::STATUS_PENDING),
		]);
		return $this;
	}

	public function approve()
	{
		return $this->setStatus(self::STATUS_APPROVED);
	}

	public function deny()
	{
		return $this->setStatus(self::STATUS_NOT_APPROVED);
	}

	public function setChild()
	{
		return $this->setStatus(self::STATUS_CHILD_FILE);
	}

	private function setStatus($status)
	{
		$this->status = $status;
		return $this->save(false);
	}

	/**
	 * Return tags from all approved files
	 * @return array
	 */
	static public function getTags()
	{
		$query = array(
			"tags"   => array('$ne' => null),
			"status" => Files::STATUS_APPROVED
		);
		$tags = db()
			->command(array(
					"distinct" => "files",
					"key"      => "tags",
					"query"    => $query)
			);
		$tags = $tags['values'];
		return is_null($tags) ? array() : $tags;
	}

	static public function humanSize($b)
	{
		return round($b / 1024 / 1024, 2) . ' Mb';
	}

	static public function getPreview($id, $size = 's')
	{
		$path = Yii::getPathOfAlias(param('preview')['path']) . DIRECTORY_SEPARATOR . $size . $id . '.jpg';
		if (file_exists($path)) {
			return str_replace(Yii::getPathOfAlias('webroot'), '', $path);
		}
		return 'http://placehold.it/120x160';
	}
}

