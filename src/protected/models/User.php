<?php
class User extends EMongoDocument
    {
      public $name;
      public $languages;

      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }

      // This method is required!
      public function getCollectionName()
      {
        return 'users';
      }

      public function rules()
      {
        return array(
            array('name, languages', 'required'),
        );
      }

      public function attributeLabels()
      {
        return array(
          'languages'  => 'User Login',
          'name'   => 'Full name',
        );
      }
    }

