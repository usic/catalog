<?php


class QueueCommand extends CConsoleCommand {

	public function actionSaveFiles() {
		$queue = Yii::app()->yiinstalk->getClient('storage');
		$queue->watch('files');

		while ($job = $queue->reserve()) {
			$data = CJSON::decode($job->getData());
			$model = new FileStorage();
			$model->setAttributes($data, false);
			$model->setFile($model->file);
			if ($model->save(false)) {
				unlink($model->filename);
				$queue->delete($job);
			} else {
				$queue->bury($job);
			}
		}
	}

} 