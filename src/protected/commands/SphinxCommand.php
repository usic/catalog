<?php

class SphinxCommand extends CConsoleCommand
{
	public function actionIndex()
	{
		$allowedTypes = array(
			'pdf'  => array('application/pdf'),
			'docx' => array('application/zip'),
			'doc'  => array('application/vnd.ms-office', 'application/msword'),
			'txt'  => array('text/plain'),
			'html' => array('text/plain')
		);

		$pdfInfoPath = '/usr/bin/pdfinfo';
		$pdfToTextPath = '/usr/bin/pdftotext';
		$catDocPath = '/usr/bin/catdoc';
		$catPath = '/bin/cat';

		$documents = Files::model()->findAllByAttributes(array(
			'status' => Files::STATUS_APPROVED,
		));

		$xmlWriter = new xmlWriter();
		$xmlWriter->openMemory();
		$xmlWriter->setIndent(true);
		$xmlWriter->startDocument('1.0', 'UTF-8');

		$xmlWriter->startElement('sphinx:docset');

		$xmlWriter->startElement('sphinx:schema');
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'content');
		$xmlWriter->endElement(); // field
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'tags');
		$xmlWriter->endElement(); // field tags
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'courses');
		$xmlWriter->endElement(); // field courses
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'title');
		$xmlWriter->endElement(); // field title
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'description');
		$xmlWriter->endElement(); // field description
		$xmlWriter->startElement('sphinx:attr');
		$xmlWriter->writeAttribute('name', 'rating');
		$xmlWriter->writeAttribute('type', 'float');
		$xmlWriter->endElement(); // field rating
		$xmlWriter->startElement('sphinx:attr');
		$xmlWriter->writeAttribute('name', 'user');
		$xmlWriter->writeAttribute('type', 'string');
		$xmlWriter->endElement(); // field user
		$xmlWriter->startElement('sphinx:field');
		$xmlWriter->writeAttribute('name', 'authors');
		$xmlWriter->endElement(); // field authors
		$xmlWriter->startElement('sphinx:attr');
		$xmlWriter->writeAttribute('name', 'year');
		$xmlWriter->writeAttribute('type', 'int');
		$xmlWriter->endElement(); // field year
		$xmlWriter->endElement(); // schema

		$gridfs = Yii::app()->mongodb->getDb()->getGridFS('storage'); //get GRIDFS

		foreach ($documents as $doc) {
			$id = sprintf('%u', crc32(strval($doc['_id'])));
			$res = $gridfs->
				findOne(array('_id' => $doc['_id']));
			//get extension from database and check it
			$tmp_name = tempnam(sys_get_temp_dir(), 'CATALOG');
			file_put_contents($tmp_name, $res->getBytes());
			$text = '';
			$filePath = $tmp_name;
			$mime = $res->file['mime'];
			$temp = explode('.', $doc['name']);
			$ext = $temp[1];
			if (array_key_exists($ext, $allowedTypes) && (in_array($mime, $allowedTypes[$ext]))) {
				$filePathEscape = escapeshellarg($filePath);
				try {
					$mime = ($ext == 'docx') ? 'application/zip' : $mime;
					switch ($mime) {
						case 'application/pdf':
						{
							$pdfInfo = array();
							$key = '';
							$val = '';

							foreach (explode("\n", shell_exec($pdfInfoPath . ' ' . $filePathEscape)) as $str) {
								list($key, $val) = count(explode(':', $str)) == 2 ? explode(':', $str) : array('', '');
								if (trim($key) && trim($val)) {
									$pdfInfo[trim($key)] = trim($val);
								}
							}
							// check errors
							if (empty($pdfInfo) || (isset($pdfInfo['Error']) && $pdfInfo['Error'])) {
								continue;
							}

							$text = shell_exec($pdfToTextPath . ' -nopgbrk ' . $filePathEscape . ' -');
							break;
						}
						case 'application/zip' :
						{
							$objZip = new ZipArchive();
							$objZip->open($filePath);
							$text = $objZip->getFromName('word/document.xml');
							break;
						}

						case ('application/vnd.ms-office' || 'application/msword'):
						{
							$text = shell_exec(escapeshellcmd($catDocPath . ' ' . $filePathEscape));
							break;
						}
						case('text/plain'):
						{
							$text = shell_exec(escapeshellcmd($catPath . ' ' . $filePathEscape));
							break;
						}
					}
					$text = strip_tags($text);
					$givenEncode = mb_detect_encoding($text);
					// Text must have UTF-8 encoding
					$text = $givenEncode ? iconv($givenEncode, 'UTF-8', $text) : mb_convert_encoding($text, 'UTF-8');

				} catch (Exception $e) {
					echo $e->getMessage() . PHP_EOL;
					continue;
				}
			}
			$xmlWriter->startElement('sphinx:document');
			$xmlWriter->writeAttribute('id', $id);
			$xmlWriter->startElement('content');
			$xmlWriter->writeCData($text);
			$xmlWriter->endElement(); // content

			$xmlWriter->startElement('tags');
			$xmlWriter->writeCData(implode(',', $doc['tags']));
			$xmlWriter->endElement(); // tags

			$courses = (is_array($doc['courses'])) ? $doc['courses'] : array();
			$xmlWriter->startElement('courses');
			$xmlWriter->writeCData(implode(',', $courses));
			$xmlWriter->endElement(); // courses

			$xmlWriter->startElement('description');
			$xmlWriter->writeCData($doc['description_mkd']);
			$xmlWriter->endElement(); // description markdown

			$xmlWriter->startElement('user');
			$xmlWriter->writeRaw($doc['user']);
			$xmlWriter->endElement(); // user

			$xmlWriter->startElement('authors');
			$xmlWriter->writeCData($doc['authors']);
			$xmlWriter->endElement(); // authors

			$xmlWriter->startElement('year');
			$xmlWriter->writeRaw($doc['year']);
			$xmlWriter->endElement(); // year

			$xmlWriter->startElement('title');
			$xmlWriter->writeCData($doc['title']);
			$xmlWriter->endElement(); // title

			$xmlWriter->endElement(); // document
			//update document and set it indexed, add sphinx id
			Files::model()->updateByPk($doc['_id'], array('$set' => array('sphinx_id' => new MongoInt64($id))));
			unlink($tmp_name);
		}
		$xmlWriter->endElement();
		$xml = $xmlWriter->outputMemory();

		$tidy = tidy_repair_string($xml, array(
			'output-xml' => true,
			'input-xml'  => true
		), 'utf8');
		echo $tidy;
	}
}