<?php

class CListDataHelper
{
	public static function getData($category)
	{
		$cmap = new CMap();
		$arrayDataFile = Yii::getPathOfAlias('application.helpers.data') . DS . strtolower($category) . '.php';
		$arrayData = file_exists($arrayDataFile) ? require($arrayDataFile) : null;
		$cmap->mergeWith($arrayData);
		return $cmap;
	}

	public static function getListData($category, $firstItemEmpty = false)
	{
		$list = new CMap();
		if ($firstItemEmpty) {
			$list->add('', '');
		}
		$data = self::getData($category);
		$list->mergeWith($data);
		$result = array();
		foreach ($list as $k => $item) {
			$result[$k] = is_array($item) ? $item[0] : $item;
		}
		return $result;
	}

	public static function getLabel($category, $value, $options = array())
	{
		$data = self::getData($category);
		return (is_array($data[$value])) ? $data[$value][0] : $data[$value];
	}
}
