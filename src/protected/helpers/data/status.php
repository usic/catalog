<?php
return array(
	Files::STATUS_APPROVED     => array('Підтверджено', 'options' => array('color' => '')),
	Files::STATUS_NOT_APPROVED => array('Не підтверджено'),
	Files::STATUS_PENDING      => array('Очікується підтвердження')
);