<?php if(count($comments) == 0): ?>
	<div> Прокоментуйте першими </div>
<?php else: ?>
	<ul>
		<?php foreach($comments as $comment): ?>
			<li>
				<div><?php echo $comment['user']; ?></div>
				<div><?php echo date(' h:i:s d-M-Y', $comment['time_created']->sec); ?></div>
				<div><?php echo CHtml::encode($comment['comment']); ?></div>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>