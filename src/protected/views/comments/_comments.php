<script>
	function sendComment()
	{
		$.ajax({
			url: '<?php echo $this->createUrl('comments/add') ?>',
			type: 'POST',
			data: {
					Comment: {
						file_id: '<?php echo $file; ?>',
						comment: $('#comment').val()
					}
				  },
			dataType: 'json',
			error: function(xhr, status, error){
				alert(xhr.responseText);
			}
		}).success(function(){
			$('#comment').val('');
			reloadComments();
		});
	}

	function reloadComments()
	{
		$.ajax({
			url: '<?php echo $this->createUrl('comments/fileComments') ?>',
			type: 'GET',
			data: {
				file_id: '<?php echo $file; ?>'
			},
			dataType: 'html',
			error: function(xhr, status, error){
				alert(xhr.responseText);
			}
		}).success(function($data){
			$('#comments').empty().append($data);
		});
	}

	$(document).ready(function(){
		$('#sendComment').click(function(){
			sendComment();
		});
	})
</script>

<div id="comments">
<?php $this->renderPartial('//comments/_commentsList',array(
	'comments'=>$comments
)); ?>
</div>

<?php if(!Yii::app()->user->isGuest): ?>
	<textarea rows="5" cols="50" id="comment">
	</textarea>
	<button id="sendComment">Прокоментувати</button>
<?php endif?>


