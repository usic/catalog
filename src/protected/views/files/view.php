<article>
	<h1><?php echo CHtml::encode($model->title); ?></h1>
	<img src="<?php echo Files::getPreview(strval($model->_id), 'm'); ?>" class="pure-img left"/>
	<ul>
		<li>Year: <?php echo $model->year; ?></li>
		<li>Author: <?php echo $model->authors; ?></li>
		<li>Description:<?php echo $model->description; ?></li>
	</ul>
	<?php $this->widget('CStarRating', array(
		'name'      => 'rating',
		'value'     => '4',
		'minRating' => 1,
		'maxRating' => 10,
		'starCount' => 10,
	));?>
	<?php echo CHtml::link(Yii::t('app', 'Download'), array('/files/download', 'id' => $model->_id, 'name' => $model->name), array('target' => '_blank')); ?>

	<div>
		<?php $this->renderPartial('//comments/_comments',array(
			'comments'=>$model->comments,
			'file'=>$model->_id
		)); ?>
	</div>
</article>