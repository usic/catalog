<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'                   => 'file-form',
		'enableAjaxValidation' => false,
		'htmlOptions'          => array(
			'class' => 'pure-form pure-form-aligned'
		)
	)); ?>

	<?php echo $form->errorSummary($model); ?>
	<fieldset>
		<legend class="edit">Редагування матеріалу <b><?php echo CHtml::encode($model->name); ?></b></legend>
		<div class="inline-block">
		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'title'); ?>
			<?php echo $form->textField($model, 'title', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255, 'required' => 'required')); ?>
		</div>

		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'year'); ?>
			<?php echo $form->textField($model, 'year', array('class' => 'form-control', 'required' => 'required', 'type' => 'year')); ?>
		</div>

		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'authors'); ?>
			<?php echo $form->textField($model, 'authors', array('class' => 'form-control', 'required' => 'required')); ?>
		</div>

		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'courses'); ?>
			<?php echo $form->textField($model, 'courses');
			$this->widget('application.lib.select2.ESelect2', array(
				'name'        => 'Files_courses',
				'htmlOptions' => array(
					'class'   => 'pure-input-1'
				),
				'selector'    => '#Files_courses',
				'options'     => array(
					'placeholder'        => Yii::t('app', 'Select a subject'),
					'allowClear'         => false,
					'multiple'           => true,
					'width'              => '400px',
					'minimumInputLength' => 1,
					'ajax'               => array(
						'url'      => 'http://api.usic.at/api/v1/courses/autocomplete?limit=100',
						'dataType' => 'jsonp',
						'data'     => 'js: function(term,page) {
    						return {
    							q: term
    						};
    					}',
						'results'  => 'js: function(data,page){
							return {results: data};
						}',
					),
					'formatResult'       => 'js: function(value){
    					var markup = "<table class=\"value-result\"><tr><td>";
    					markup += "<div>"+value.course+"</div>";
    					markup += "<div>"+"Рік - "+value.year+"</div>";
    					markup += "<div>"+value.level+"</div>";
    					markup += "</td></tr></table>";
    					return markup;
    				}',
					'formatSelection'    => 'js: function(value){
    					return value.course;
    				}',
					'initSelection'      => 'js:function(element, callback) {
                        var data = [];
                        $(element.val().split(",")).each(function (k,v) {
                            data.push({id: v, course: v});

                        });
                        callback(data);
                    }'
				),
			));
			?>
		</div>

		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'category', array('id' => 'checkbox-inline')); ?>
			<?php echo $form->radioButtonList($model, 'category', CListDataHelper::getListData('category'), array(
					'separator'    => "",
					'labelOptions' => array('style' => 'display:inline'),
				 	'required'       => 'required'
				)
			); ?>
		</div>

		<div class="pure-control-group form-group">
			<?php echo $form->labelEx($model, 'description_mkd', array('class' => 'comment-label')); ?>
			<?php echo $form->textArea($model, 'description_mkd', array('class' => 'form-control', 'rows' => '6' , 'required' => 'required')); ?>
		</div>

		<div class="pure-controls save">
			<?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'btn btn-default')); ?>
		</div>
		</div>
	</fieldset>
	<?php $this->endWidget(); ?>

</div><!-- form -->
