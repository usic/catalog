<?php
/* @var $this SiteController */
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/dropzone.css');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/dropzone.min.js',CClientScript::POS_END);
$this->pageTitle = 'Завантаження файлу - ' . Yii::app()->name;
?>
<?php if (Yii::app()->user->hasFlash('error')): ?>
	<div class="info">
		<?php echo Yii::app()->user->getFlash('error'); ?>
	</div>
<?php endif; ?>

<section>
	<div id="dropzone">
		<form action="<?php echo $this->createUrl('files/upload'); ?>" class="dropzone dz-clickable" id="my-dropzone">
			<div class="dz-default dz-message">
				<span>(або клікніть)</span>
			</div>
		</form>
	</div>
</section>

