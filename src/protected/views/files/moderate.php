<?php

$this->renderPartial("_form", ['model' => $model]);

if(isset($model->parent))
	echo CHtml::link("Дочірній файл", $this->createUrl('files/view', ['id' => $model->_id]));

echo CHtml::ajaxButton("Підтвердити",
	$model->parent ? $this->createUrl('files/approveChild', ['id' => $model->_id]) : $this->createUrl('files/approve', ['id' => $model->_id]),
	[
		'type'=>'GET',
		'dataType'=>'json',
		'success'=> 'function(){ alert("Підтверджено");s}',
		'error'=>'function(data){ alert(data.error);}'
	]);

echo CHtml::ajaxButton("Відхилити",
	$this->createUrl('files/deny', ['id' => $model->_id]),
	[
		'type'=>'GET',
		'dataType'=>'json',
		'success'=> 'function(){ alert("Відхилено");s}',
		'error'=>'function(data){ alert(data.error);}'
	]);