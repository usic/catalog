<book class="pure-u-xl-1-3 pure-u-md-1-2 pure-u-sm-1-2 pure-u-1">
	<span
		class="title"><h4><?php echo CHtml::link(CHtml::encode($data->title), array('/files/view', 'id' => strval($data->_id))); ?>  </h4></span>

	<div class="pure-g">
		<div class="pure-u-1-2">
			<img src="<?php echo Files::getPreview(strval($data->_id)); ?>" class="pure-img">
		</div>
		<div class="pure-u-1-2">
			<ul>
				<li class="category <?php echo $data->category; ?>"><?php echo CListDataHelper::getLabel('category', $data->category); ?></li>
				<li class="year"><?php echo $data->year; ?> рік</li>
				<li title="<?php echo $data->authors; ?>"><?php echo $data->authors; ?></li>
				<li><?php echo mb_strcut($data->description, 0, 140, 'utf-8'); ?></li>
			</ul>
		</div>
	</div>
</book>