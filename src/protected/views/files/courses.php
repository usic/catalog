<?php if(count($data)<1):?>
	<?php echo Yii::t('app','Here are no any courses yet');?>
<?php else:?>
<?php
echo CHtml::openTag('ul');
foreach ($data as $course) {
	echo CHtml::openTag('li') . CHtml::link($course, array('files/course', 'id' => $course)) . CHtml::closeTag('li');
}
echo CHtml::closeTag('ul');	?>
<?php endif;?>