<?php
$this->widget('zii.widgets.CListView', array(
	'id'            => 'file-list',
	'dataProvider'  => $data,
	'itemView'      => '_view',
	'enableSorting' => false,
	'cssFile'       => false,
	'template'      => '{pager}{items}{pager}',
	'itemsCssClass' => 'pure-g',
	'pager'         => array(
		'class' => 'CustomLinkPager'
	),
));