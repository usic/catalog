<h1><?php echo Yii::t('app', 'Files to moderate'); ?></h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'           => 'files-grid',
	'pager'        => array(
		'class' => 'CustomLinkPager'
	),
	'dataProvider' => $data,
	'columns'      => array(
		array(
			'name'  => 'time_updated',
			'value' => 'date("h:i:s d-M-Y", $data->time_updated)'
		),
		array(
			'name'  => 'name',
			'value' => 'strlen($data->title)>10 ? $data->title:$data->name'
		),
		array(
			'name'  => 'fileSize',
			'value' => 'Files::humanSize($data->length)'
		),
		array(
			'name'  => 'category',
			'value' => 'CListDataHelper::getLabel("category",$data->category)'
		),
		array(
			'class'   => 'CButtonColumn',
			'buttons' => array(
				'moderate'   => array(
					'label' => 'Moderate',
					'url' => 'Yii::app()->createUrl("files/moderate", array("id" => $data->_id))',
				),
				'delete'
			),
			'template' => '{moderate}{delete}'
		)
	)
)); ?>
