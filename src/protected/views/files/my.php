<h1><?php echo Yii::t('app', 'My files'); ?></h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'           => 'files-grid',
	'pager'        => array(
		'class' => 'CustomLinkPager'
	),
	'itemsCssClass' => 'table table-hover',
	'rowCssClassExpression' => '
		($data->status == 1) ? "success" : (($data->status == 2) ? "danger" : (($data->status == 3) ? "warning" : "none"))
	',
	'dataProvider' => $data,
	'columns'      => array(
		array(
			'name'  => 'name',
			'value' => 'strlen($data->title)>10 ? $data->title:$data->name'),
		array(
			'name'  => 'fileSize',
			'value' => 'Files::humanSize($data->length)'
		),
		array(
			'name'  => 'status',
			'value' => 'CListDataHelper::getLabel("status",$data->status)'
		),
		array(
			'name'  => 'category',
			'value' => 'CListDataHelper::getLabel("category",$data->category)'
		),
		array(
			'class'   => 'CButtonColumn',
			'buttons' => array(
				'view'   => array(
					'visible' => '$data->status!=Files::STATUS_NOT_APPROVED',
					'options'=>array('title'=>'Переглянути')
				),
				'update' => array(
					'visible' => '$data->status!=Files::STATUS_APPROVED',
					'options'=>array('title'=>'Редагувати')
				),
				'delete' => array(
					'visible' => '$data->status!=Files::STATUS_APPROVED',
					'options'=>array('title'=>'Видалити')
				)
			)
		)
	)
)); ?>
