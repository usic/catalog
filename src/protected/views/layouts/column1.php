<?php
/* @var $this Controller */
$class = 'pure-u-1';
?>
<?php $this->beginContent('//layouts/main'); ?>
<?php if ($this->filter): ?>
	<?php $this->renderPartial('//layouts/filter'); ?>
	<?php $class = "pure-u-lg-4-5 pure-u-md-3-4 pure-u-1"; ?>
<?php endif; ?>
	<div class="<?php echo $class; ?>" id="content">

		<?php echo $content; ?>

	</div>
<?php $this->endContent(); ?>