<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();

		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile($baseUrl . '/js/typeahead.js');
		$cs->registerScriptFile($baseUrl . '/js/jquery.cookie.js');
		$cs->registerScriptFile($baseUrl . '/js/catalog.js');
		$cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
	?>
	<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/styles.css">
	<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/grids-responsive-min.css">
	<link rel="shortcut icon" href="<?php echo $baseUrl; ?>/images/favicon.png" type="image/png">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
		<div class="container">
			<div class="navbar-header title pure-u-1 pure-u-sm-2-5 pure-u-md-1-3 pure-xl-1-3">
				<a href="/">
					<div class="logotype"></div>
					<div class="title-text">
						<h2>USIC</h2>
						<h1 class="green_text">catalog</h1>
					</div>
				</a>
			</div>
			<div class="right pure-u-lg-1-4 pure-u-md-1-3 pure-xl-1-3">
				<div class="social">
					<a href="http://vk.com/share.php?url=http://cat.usic.at&title=usic Catalog — каталог навчальних матеріалів"
					   title="Поділитись посиланням вконтакті"></a>
					<a href="http://www.facebook.com/sharer.php?u=http://cat.usic.at"
					   title="Поділитись посиланням на facebook"></a>
					<a href="http://twitter.com/share?url=http://cat.usic.at&text=usic Catalog — каталог навчальних матеріалів"
					   title="твітнути посилання"></a>
					<a href="http://feedback.usic.at" title="написати розробникам проекту"></a>
				</div>

			</div>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="sr-only">Toggle navigation</span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
          	<div class="navbar-collapse collapse">
            	<?php $this->widget('zii.widgets.CMenu', array(
            		'htmlOptions' => array('class' => 'nav navbar-nav'),
					'items' => array(
					array('label' => 'Все', 'url' => '/', 'active' => Yii::app()->controller->id == 'files' and Yii::app()->controller->action->id == 'index'),
					array('label' => 'Рекомендоване', 'url' => array('files/recommended'), 'visible' => !Yii::app()->user->isGuest),
					array('label' => 'Література до курсів', 'url' => array('files/courses'), 'visible' => !Yii::app()->user->isGuest),
					array('label' => 'Моє', 'url' => array('files/my'), 'visible' => !Yii::app()->user->isGuest),
					array('label' => 'Додати', 'url' => array('files/new'), 'visible' => !Yii::app()->user->isGuest),
					array('label' => 'Вхід', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
					array('label' => 'Пошук', 'url' => '#', 'linkOptions'=>array('class'=>"search"), 'visible' => !Yii::app()->user->isGuest),
					),
				));?>
				<div class="slideup form-wrapper" id="wrapper">
					<form class="navbar-form navbar-right">
						<input type="text" placeholder="пошук по каталогу..." class="form-control">
						<input type="submit" value="" placeholder="Шукати!">
					</form>
				</div>
        	</div>
		</div>
	</div>
	<!--navbar-->

	<div class="content">
		<?php echo $content; ?>
	</div>
	<footer>
		<hr>
		<a href="/tou"><h3>умови використання</h3></a>
		<a href="/help"><h3>довідка</h3></a>
		<a href="http://usic.at/"><h3>Ukma Student Internet Center</h3></a>
	</footer>
</body>
</html>