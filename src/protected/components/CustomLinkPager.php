<?php

Class CustomLinkPager extends CLinkPager
{
	public $cssFile = false;
	public $firstPageCssClass = 'page first';
	public $previousPageCssClass = 'page dark';
	public $internalPageCssClass = 'page dark';
	public $lastPageCssClass = 'page last';
	public $nextPageCssClass = 'page dark';
	public $selectedPageCssClass = 'active';
	public $firstPageLabel = '<<';
	public $prevPageLabel = '&#171;';
	public $nextPageLabel = '&#187;';
	public $lastPageLabel = '>>';
	public $header = '';
	public $maxButtonCount = 5;

	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if ($hidden || $selected) {
			$class .= ' ' . ($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
		}
		if ($selected) {
			return CHtml::tag('span', array('class' => $class), $label);
		}
		return CHtml::link($label, $this->createPageUrl($page), array('class' => $class));
	}

	public function run()
	{
		$this->registerClientScript();
		$buttons = $this->createPageButtons();
		if (empty($buttons)) {
			return;
		}
		echo $this->header;
		echo implode("\n", $buttons);
		echo $this->footer;
	}
}