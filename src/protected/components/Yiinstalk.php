<?php

/**

 * ...
 * 'components' => array(
 *   'yiinstalk' => array(
 *     'class' => 'Yiinstalk',
 *     'connections' => array(
 *       'default' => array(
 *         'host' => '127.0.0.1',
 *         'port' => 11300,
 *       ),
 *     ),
 *   ),
 * )
 * ...

 */
class Yiinstalk extends CApplicationComponent
{

  public $connections;

  protected $_clients = array();


  public function init()
  {
    parent::init();

    if (!is_array($this->connections))
      $this->connections = array();

    if (!class_exists('Pheanstalk', false))
      $this->registerAutoloader();
  }


  public function getClient($connectionName = 'default')
  {
    if (!isset($this->_clients[$connectionName])) {
      if (!array_key_exists($connectionName, $this->connections))
        throw new CException('Invalid connection name.');

      $connection = $this->connections[$connectionName];
      if (!isset($connection['port']))
        $connection['port'] = Pheanstalk_Pheanstalk::DEFAULT_PORT;
      $client = new Pheanstalk_Pheanstalk($connection['host'], $connection['port'],
        isset($connection['connectTimeout']) ? $connection['connectTimeout'] : null);

      $this->_clients[$connectionName] = $client;
    }

    return $this->_clients[$connectionName];
  }

  protected function registerAutoloader()
  {
    $classesPath = Yii::app()->basePath . '/lib/pda/pheanstalk/classes';
    require_once($classesPath . '/Pheanstalk/ClassLoader.php');
	Pheanstalk_ClassLoader::register($classesPath);
  }
}

