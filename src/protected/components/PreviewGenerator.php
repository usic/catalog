<?php
Yii::import("application.lib.image.*");

/*
* Generate previews from pdf, gif, png, jpg files.
* 
* !!!
* THIS SHOULD BE INSTALLED: php5-gd, ghostscript
* !!!
*
* Use class.upload.php (upload.php) to format result.
* Use GhostScript to convert from pdf to jpg.
* 
* Config values: previewPath, tempPath, previewHeight, previewWidth.
*
* use generateById($id) to generate file preview from file in DB
*
* $id - id of original file in MongoDB (preview file name)
*
*
*/

class PreviewGenerator
{

	private $tempPath;
	public $previewPath;

	public $height;
	public $width;

	public $prefix = '';

	public function __construct()
	{
		$this->tempPath = sys_get_temp_dir() . '/';
	}

	public function __get($property)
	{
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value)
	{
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}

	public function generateById($id)
	{
		$this->checkConfig();

		$gridFs = db()->getGridFS("storage");
		$fileGF = $gridFs->findOne(array('_id' => new MongoId($id)));
		$fileWriteName = $this->tempPath . $id;
		$fileGF->write($fileWriteName);
		if (file_exists($fileWriteName)) {
			$this->generate($fileWriteName, $id, $fileGF->file['mime']);
		} else {
			throw new CException("Can`t create file");
		}
	}

	private function generate($file, $id, $mime)
	{

		if (!is_readable($file)) {
			throw new CException('File is not exist or readable');
		}

		if ($mime == "application/pdf") {
			$file = preg_replace("/ /", '\ ', $file);
			$newFile = $this->previewPath . $id . "temp.jpg";
			exec("gs -dNOPAUSE -dBATCH -sDEVICE=jpeg -dFirstPage=1 -dLastPage=1 -sOutputFile=" . $newFile . " -dJPEGQ=100 -q " . $file . " -c quit");
			$this->make($newFile, $id);
		}
		if ($mime == "image/gif" || $mime == "image/png" || $mime == "image/jpeg") {
			$this->make($file, $id);
		}

	}

	private function checkConfig()
	{
		if (!isset($this->previewPath) || !isset($this->height) || !isset($this->width)) {
			throw new CException('Not all parameters');
		}
		
		if (!is_writable($this->previewPath) || !is_dir($this->previewPath) || !is_writable($this->tempPath) || !is_dir($this->tempPath)) {
			throw new CException('Path dir is not exist or writable');
		}
	}

	private function make($file, $id)
	{
		$handle = new upload($file);
		$handle->file_new_name_body = $this->prefix . $id;
		$handle->file_overwrite = true;
		$handle->image_convert = 'jpg';
		$handle->file_new_name_ext = 'jpg';
		$handle->image_resize = true;
		$handle->image_ratio_fill = true;
		$handle->image_x = $this->width;
		$handle->image_y = $this->height;


		$handle->process($this->previewPath);
		if ($handle->processed) {
			$handle->clean();
		} else {
			throw new CException($handle->error);
		}
	}

}