<?php

class UsicUser extends CWebUser
{

	public $identityCookie = array(
		'path'     => '/',
		'httpOnly' => true
	);

	public function init()
	{
		$this->identityCookie['domain'] = ((PHP_SAPI != 'cli') ? substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], '.')) : false);
		$this->autoRenewCookie = true;
		$this->setStateKeyPrefix('');
		parent::init();
	}

	public function getIsGuest()
	{
		return $this->getState('id') === null;
	}


	public function getId()
	{
		return $this->getState('id');
	}


	public function setId($value)
	{
		$this->setState('id', $value);
	}

	public function getName()
	{
		if (($name = $this->getState('name')) !== null) {
			return $name;
		} else {
			return $this->guestName;
		}
	}

	public function setName($value)
	{
		$this->setState('name', $value);
	}

	public function inGroup($group)
	{
		return !$this->getIsGuest() && in_array($group, $this->groups);
	}
}

?>