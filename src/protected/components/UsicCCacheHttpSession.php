<?php

class UsicCCacheHttpSession extends CCacheHttpSession
{
	/**
	 * @param string $id
	 * @return string
	 * @description overwrites default method with prefix
	 */
	public function calculateKey($id)
	{
		return $id;
	}
}