<?php

class FileMimeToSize extends CValidator{

	public $defaultSize;
	public $mimeTypes;

	protected function validateAttribute($object, $attribute)
	{
		$mimes = $this -> getMimes();
		$validator = CValidator::createValidator('file', $object, $attribute, array('allowEmpty' => false, 'mimeTypes' => $mimes, 'wrongMimeType' => Yii::t('app', 'Wrong mime type')));
		$validator->validate($object);

		$file = $object->$attribute;
		if (array_key_exists($file->getType(), $this->mimeTypes) && $file->getSize() > $this->mimeTypes[$file->getType()]) {
			$this->addError($object, $attribute, "File is too big");
		} else if (!array_key_exists($file->getType(), $this->mimeTypes) && $file->getSize() > $this->defaultSize) {
			$this->addError($object, $attribute, "File is too big");
		}

	}

	private function getMimes()
	{
		$mimes = array();
		foreach ($this->mimeTypes as $key => $value) {
			if (is_int($key)) {
				array_push($mimes, $value);
			} else {
				array_push($mimes, $key);
			}
		}

		return $mimes;
	}

}