<?php

class FilesController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete,confirm,vote', // we only allow deletion via POST request
		);
	}

	private $_accessExpresions = [
		'toModerate' => 'Yii::app()->user->inGroup("nimda")',
		'toVoteDown' => 'Files::model()->findByAttributes(array("user" => Yii::app()->user->name, "status" => Files::STATUS_APPROVED)) != null', //TODO More efficient
	];

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'view', 'popular', 'search', 'course', 'courses', 'quickSearch'),
				'users'   => array('*'),
			),
			array(
				'allow',
				'actions' => array('new', 'upload', 'recommended', 'my', 'voteUp', 'download'),
				'users'   => array('@'),
			),
			array(
				'allow',
				'actions' => array('update', 'delete'),
				'users'   => array('@')
			),
			array(
				'allow',
				'actions'    => array('voteDown'),
				'users'      => array('@'),
				'expression' => $this->_accessExpresions['toVoteDown']
			),
			array(
				'allow',
				'actions'    => array('moderateList', 'moderate', 'approve', 'approveChild', 'deny'),
				'users'      => array('@'),
				'expression' => $this->_accessExpresions['toModerate']
			),
			array(
				'deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * @param string $query
	 */
	public function actionIndex($query = '')
	{
		$this->filter = true;
		$dataProvider = new EMongoDataProvider(Files::model()->approved(), array(
			'pagination' => array(
				'pageSize' => 5
			)
		));
		$this->render(
			'index', array(
				'data' => $dataProvider
			)
		);
	}

	/**
	 * Renders form form uploading
	 * or display dialog
	 */
	public function actionNew()
	{
		$this->render('_upload_form');
	}

	/** MODERATION */


	public function actionModerateList()
	{
		$dataProvider = new EMongoDataProvider(Files::model()->notApproved());
		$this->render(
			'moderateList', array(
				'data' => $dataProvider
			)
		);
	}

	/**
	 * Mark file as approved.
	 * Only files with full
	 * info can be approved
	 */
	public function actionApprove($id)
	{
		/* action can be preformed only by moderators */
		$file = $this->loadModel($id);
		if (!$file->approve()) {
			throw new CHttpException(418, "Model can not be saved");
		}
	}

	public function actionDeny($id)
	{
		/* action can be preformed only by moderators */
		//TODO Send message to user
		$file = $this->loadModel($id);
		if (!$file->deny()) {
			throw new CHttpException(418, "Model can not be saved");
		}
	}

	public function actionApproveChild($id)
	{
		/* action can be preformed only by moderators */
		//TODO Send message to user
		$file = $this->loadModel($id);
		if (!$file->setChild()) {
			throw new CHttpException(418, "Model can not be saved");
		}
	}

	public function actionModerate($id)
	{
		$file = $this->loadModel($id);

		if (isset($_POST['Files'])) {
			$file->setScenario('update');
			$file->attributes = $_POST['Files'];
			if ($file->validate() && $file->save()) {
				echo CJSON::encode(['success' => Yii::t('app', 'File information updated')]);
			}
		}

		$this->render(
			'moderate', array(
				'model' => $file
			)
		);
	}

	/* MODERATION END */
	/**
	 * Displays found file's data
	 *
	 * @param $q
	 *
	 */
	public function actionSearch($q)
	{
		if (empty($q) || strlen($q) < 3) {
			echo CJSON::encode(array('error' => Yii::t('app', 'The query length can not be less then 3 chars')));
			Yii::app()->end();
		}
		if (!extension_loaded("sphinx")) {
			echo CJSON::encode(
				array(
					'class_error' => Yii::t('app', 'No sphinx module is available.It must be installed and enable to use search.')
				)
			);
			Yii::app()->end();
		}
		$criteria = new ESphinxSearchCriteria(array(
			'sortMode'   => ESphinxSort::RELEVANCE,
			'matchMode'  => ESphinxMatch::PHRASE,
			'maxMatches' => 50,
			'limit'      => 10
		));

		$query = new ESphinxQuery($this->escapeString($q), 'books', $criteria); // search in all fields
		$sq_result = Yii::app()->sphinx->executeQuery($query);

		if ($sq_result->count() == 0) {
			echo CJSON::encode(array('no_results_found' => Yii::t('app', 'Try to use custom search')));
			Yii::app()->end();
		}

		for ($i = 0; $i < $sq_result->count(); $i++) {
			if ($sq_result->offsetExists($i)) {
				$item = $sq_result->offsetGet($i);
				$ids[] = new MongoInt64($item->id);
				$weights[$item->id] = $item->weight;
			}
		}
		//use code below to get data from database
		$data = db()->storage->files->find(
			array('sphinx_id' => array('$in' => $ids)),
			array(
				'title'     => 1,
				'authors'   => 1,
				'sphinx_id' => 1
			)
		)
			->limit(10);
		if (iterator_count($data) == 0) {
			echo CJSON::encode(array('no_data_to_return' => Yii::t('app', 'Try to use search in few minutes')));
			Yii::app()->end();
		}
		//this must be simplest
		foreach ($weights as $id => $weight) {
			foreach ($data as $doc_id => $document) {
				if ($id == $document['sphinx_id']) {
					$document['id'] = strval($document['_id']);
					unset($document['_id']);
					unset($document['sphinx_id']);
					$result[] = $document;
				}
			}
		}
		echo CJSON::encode($result);
	}

	public function actionRecommended()
	{
		$this->render('index', ['data' => new EMongoDataProvider(Files::model()->recently()->approved(), [
			'pagination' => false
		])]);
	}

	/**
	 * @return latest files
	 */
	public function actionHot()
	{
		$this->render('index', [
				'data' => new EMongoDataProvider(Files::model()->recently()->approved(), [
						'pagination' => false
					]
				)
			]
		);
	}

	/**
	 * Renders user's file list
	 */
	public function actionMy()
	{
		$c = new EMongoCriteria;
		$c->addCondition('user', Yii::app()->user->login);
		$c->setSort(array('uploadDate' => -1));
		$dataProvider = new EMongoDataProvider('Files', array(
			'criteria' => $c,
		));
		$this->render(
			'my', array(
				'data' => $dataProvider
			)
		);
	}

	private function vote($type)
	{
		switch ($type) {
			case 'up':
				$voteNumber = 1;
				$votePlus = 2;
				break;
			case 'down':
				$voteNumber = -1;
				$votePlus = -2;
				break;
		}
		$file = Files::model()->findByPk(new MongoId(CHttpRequest::getParam('id')));
		if ($file != null) {
			$user = Yii::app()->user->name;
			if (isset($file->votes)) {
				foreach ($file->votes as $vote) {
					if ($vote->user == $user && $vote->vote != -($voteNumber)) {
						echo CJSON::encode(array('error' => Yii::t('app', 'You can`t vote anymore')));
						Yii::app()->end();
					} elseif ($vote->user == $user) {
						$voteNumber = $votePlus;
					}
				}
			}
			$file->votes[] = new FileVote;
			end($file->votes)->user = $user;
			end($file->votes)->vote = $voteNumber;
			end($file->votes)->created = new MongoDate();
			$file->save();
			echo CJSON::encode(array('success' => Yii::t('app', 'Thank you for voting')));
		} else {
			echo CJSON::encode(array('error' => Yii::t('app', 'No such file')));
		}
		Yii::app()->end();
	}

	public function actionVoteUp()
	{
		$this->vote('up');
	}

	public function actionVoteDown()
	{
		$this->vote('down');
	}

	/**
	 * Out file stream to browser,
	 * send headers for saving file,
	 * increment download counter
	 *
	 * @param $id
	 *
	 * @throws CHttpException
	 */
	public function actionDownload($id)
	{
		$f = FileStorage::model()->findByPk(new MongoId($id));
		if (is_null($f)) {
			throw new CHttpException(404, Yii::t('app', 'Requested file does not exists.'));
		}
		if ($f->getFile()->file['status'] == Files::STATUS_NOT_APPROVED) {
			throw new CHttpException(403, Yii::t('app', 'This file has not been approved yet.'));
		}
		$stream = $f->getFile()->getResource();
		header('Content-Description: File Transfer');
		header('Content-type: ' . $f->getFile()->file['mime']);
		header('Content-Disposition: attachment; filename=' . ($f->getFile()->file['name']));
		header('Content-Transfer-Encoding: binary');
		header('Pragma: public');
		header('Content-Length: ' . $f->getFile()->getSize());
		while (!feof($stream)) {
			echo fread($stream, 8192);
		}
		Files::model()->updateByPk($f->_id, array('$inc' => array("downloads" => 1)));
	}

	/**
	 * Renders single document
	 *
	 * @param $id
	 *
	 * @throws 403 error if status not_approved
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if ($model->status == Files::STATUS_NOT_APPROVED) {
			throw new CHttpException('403', Yii::t('app', 'Access denied to not approved materials'));
		}
		$this->render('view', array('model' => $model));
	}

	/**
	 * Delete single document
	 *
	 * @param $id
	 *
	 * @throws CHttpException
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if (!$this->_isOwner($model) || $model->status == Files::STATUS_APPROVED) {
			throw new CHttpException(403, Yii::t('app', 'You can not delete this material'));
		}
		try{
			$model->delete();
			echo CJSON::encode(['status' => 'success']);
		}
		catch(Exception $e){
			echo CJSON::encode(['status' => 'error','message'=>$e->getMessage()]);
		}
	}

	/**
	 * Handle post request,
	 * output status in json format
	 */
	public function actionUpload()
	{
		header('Content-Type: application/json');
		if ($file = CUploadedFile::getInstanceByName('file')) {
			$model = new FileStorage();
			$model->file = $file;
			if (isset($_POST['parent'])) {
				$model->parent = $_POST['parent'];
				$model->setScenario('childFile');
			} else {
				$model->setScenario('checkFile');
			}
			if ($model->validate()) {
				unset($model->file);
				$model->setScenario('insert');
				$file->saveAs($file->getTempName());
				$model->name = $file->getName();
				$model->mime = $file->getType();
				$model->setFile($file->getTempName());
				$model->addFileToQueue();
			} else {
				throw new CHttpException('418', CJSON::encode(array('errors' => $model->getErrors())));
			}
		}
		echo CJSON::encode(['status'=>'success']);
	}

	/**
	 * Update document
	 *
	 * @param $id
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (!$this->_isOwner($model)) {
			throw new CHttpException(403, 'You are not allowed no update this file');
		}

		if (isset($_POST['Files'])) {
			$model->setScenario('update');
			$model->attributes = $_POST['Files'];
			if ($model->validate() && $model->save()) {
				Yii::app()->user->setFlash('message', Yii::t('app', 'File information updated'));
				$this->redirect('/files/my');
			}
		}

		$this->render(
			'_form', array(
				'model' => $model
			)
		);
	}

	/**
	 * @throws CHttpException
	 * @description View all available courses with materials
	 */
	public function actionCourses()
	{
		$courses = Yii::app()->mongodb->aggregate(
			'storage.files',
			array(
				array('$match' => array('status' => Files::STATUS_APPROVED)),
				array('$project' => array('a' => '$courses.course')),
				array('$unwind' => '$a'),
				array('$group' => array('_id' => '1', 'courses' => array('$addToSet' => '$a'))),
			)
		);
		$c = array();
		if (!empty($courses['result'])) {
			$c = $courses['result']['0']['courses'];
			uasort($c, array('self', 'compare'));
		}
		$this->render(
			'courses', array(
				'data' => $c
			)
		);
	}

	/**
	 * Files by course
	 *
	 */
	public function actionCourse($course)
	{
		$c = new EMongoCriteria;
		$c->setCondition(array('courses' => array('$elemMatch' => array('course' => $course))));
		$c->addCondition("status", Files::STATUS_APPROVED);
		$c->setSort(array('rating' => -1));
		$dataProvider = new EMongoDataProvider('Files', array(
			'criteria' => $c,
		));
		$this->render(
			'index', array(
				'data' => $dataProvider
			)
		);
	}

	public function actionQuickSearch($q)
	{
		$files = Files::model()->find(array(
			"name"   => new MongoRegex("/^$q/i"),
			"status" => Files::STATUS_APPROVED
		), array("name" => true) // set null to other fields
		)->sort(array('rating' => -1));
		echo CJSON::encode(iterator_to_array($files));
	}

	/**
	 * @param $id
	 *
	 * @return MongoObject
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Files::model()->findByPk(new MongoId($id));
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	public static function compare($a, $b)
	{
		$la = self::mb_ord(mb_substr($a, 0, 1, 'utf-8'));
		$lb = self::mb_ord(mb_substr($b, 0, 1, 'utf-8'));
		if ($la == 1028 && $lb < 1046 && $lb >= 1040) { // for YE letter
			return 1;
		}
		if ($la == 1028 && $lb >= 1046) {
			return -1;
		}
		if ($la == 1030 && $lb <= 1048 && $lb != 1031) { // for I letter
			return 1;
		}
		if ($la == 1030 && ($lb == 1031 || $lb > 1048)) {
			return -1;
		}
		if ($la == 1031 && $lb <= 1048) { // for YI letter
			return 1;
		}
		if ($la == 1031 && $lb > 1048) {
			return -1;
		}
		return $la > $lb ? 1 : -1;

	}

	/**
	 * @param $string
	 *
	 * @return int
	 */
	public static function mb_ord($string)
	{
		if (extension_loaded('mbstring') === true) {
			mb_language('Neutral');
			mb_internal_encoding('UTF-8');
			mb_detect_order(array('UTF-8', 'ISO-8859-15', 'ISO-8859-1', 'ASCII'));
			$result = unpack('N', mb_convert_encoding($string, 'UCS-4BE', 'UTF-8'));
			if (is_array($result) === true) {
				return $result[1];
			}
		}
		return ord($string);
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function mb_html_entity_decode($string)
	{
		if (extension_loaded('mbstring') === true) {
			mb_language('Neutral');
			mb_internal_encoding('UTF-8');
			mb_detect_order(array('UTF-8', 'ISO-8859-15', 'ISO-8859-1', 'ASCII'));

			return mb_convert_encoding($string, 'UTF-8', 'HTML-ENTITIES');
		}

		return html_entity_decode($string, ENT_COMPAT, 'UTF-8');
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function mb_chr($string)
	{
		return self::mb_html_entity_decode('&#' . intval($string) . ';');
	}

	private function escapeString($string)
	{
		$from = array('\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=');
		$to = array('\\\\', '\(', '\)', '\|', '\-', '\!', '\@', '\~', '\"', '\&', '\/', '\^', '\$', '\=');
		return str_replace($from, $to, $string);
	}

	private function _isOwner($model)
	{
		return $model->user === Yii::app()->user->login || Yii::app()->user->inGroup('nimda');
	}

}
