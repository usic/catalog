<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionLogin()
    {
	    header('Location: http://my'
		    . Yii::app()->session->cookieParams['domain'] .
		    '/login?from=http://cat' . Yii::app()->session->cookieParams['domain'] .
		    Yii::app()->user->returnUrl
	    );
    }

	public function actionLogout()
	{
		header('Location: http://my'
			. Yii::app()->session->cookieParams['domain'] .
			'/logout?from=http://cat' . Yii::app()->session->cookieParams['domain']
		);
	}
}
