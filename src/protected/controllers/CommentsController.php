<?php


class CommentsController extends Controller{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + add',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('fileComments'),
				'users'   => array('*'),
			),
			array(
				'allow',
				'actions' => array('add'),
				'users'   => array('@'),
			),
			array(
				'deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionAdd()
	{
		header('Content-Type: application/json');
		if(isset($_POST['Comment'])) {
			$comment = new Comment();
			$comment->setScenario("addComment");
			$comment->attributes = $_POST['Comment'];
			if($comment->validate()) {
				$comment->saveComment();
			} else {
				throw new CHttpException('400', CJSON::encode(array('error' => $comment->getErrors())));
			}
		}
	}

	public function actionFileComments($file_id)
	{
		$comment = new Comment();
		$comment->file_id = $file_id;
		if($comment->validate()){
			$this->renderPartial('_commentsList', array('comments' => $comment->getFileComments()));
		} else {
			throw new CHttpException('400', CJSON::encode(array('error' => $comment->getErrors())));
		}
	}
} 