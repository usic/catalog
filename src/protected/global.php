<?php
/**
 * @return mixed
 */
function app()
{
	return Yii::app();
}

function user()
{
	return Yii::app()->user;
}

function db()
{
	return Yii::app()->mongodb->getDb();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
	return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
	return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
	return CHtml::link($text, $url, $htmlOptions);
}


/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
	static $baseUrl;
	if ($baseUrl === null) {
		$baseUrl = Yii::app()->getRequest()->getBaseUrl();
	}
	return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name, $default = null)
{
	return isset(Yii::app()->params[$name]) ? Yii::app()->params[$name] : $default;
}

function dump($target)
{
	return CVarDumper::dump($target);
}

function existsCookie($name)
{
	return isset(Yii::app()->request->cookies[$name]->value);
}

function getCookie($name)
{
	return (isset(Yii::app()->request->cookies[$name]->value)) ? Yii::app()->request->cookies[$name]->value : '';
}

function setCookies($name, $value, $time = 0, $disableClientCookies = false)
{
	$cookie = new CHttpCookie($name, $value);
	$cookie->expire = time() + $time;
	$cookie->httpOnly = $disableClientCookies;
	Yii::app()->request->cookies[$name] = $cookie;
}


function removeCookie($name)
{
	setCookies($name, '', -3600);
}
