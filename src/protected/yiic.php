<?php

date_default_timezone_set('UTC');
$yiic = dirname(__FILE__) . '/../protected/lib/yiisoft/yii/framework/yiic.php';
require_once dirname(__FILE__) . '/../protected/config/main.php';
unset($config['defaultController']);
require_once($yiic);
