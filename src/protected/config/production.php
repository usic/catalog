<?php
return array(
	'onEndRequest' => ['StatisticsComponent', 'log'],
	'components'   => array(
		'mongodb'      => array(
			'server'  => 'mongodb://MONGODBHOST',
			'options' => array('db' => 'MONGODBNAME', 'username' => 'MONGODBUSER', 'password' => 'MONGODBPASSWORD'),
			'db'      => 'MONGODBNAME'
		),
		'redisCache'   => array(
			'servers' => array(
				array(
					'host'     => 'REDISHOST',
					'password' => 'REDISPASSWORD'
				),
			),
		),
		'sentry'       => array(
			'class'             => 'application.lib.crisu83.yii-sentry.components.SentryClient',
			'dns'               => 'SENTRYDNS',
			'environment'       => 'production',
			'yiiExtensionAlias' => 'application.lib',
			'dependencies'      => array(
				'raven'         => 'application.lib.raven.raven',
				'yii-extension' => 'application.lib.crisu83.yii-extension',
			)
		),
		'errorHandler' => array(
			'class' => 'application.lib.crisu83.yii-sentry.components.SentryErrorHandler',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'application.lib.crisu83.yii-sentry.components.SentryLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
	'params'       => array(
		'adminEmail' => 'root@usic.at'
	)
);