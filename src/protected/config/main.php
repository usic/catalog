<?php
define('DS', DIRECTORY_SEPARATOR);
$dir = dirname(__FILE__) . DS;

$shared = require $dir . 'shared.php';

if (basename(__FILE__, '.php') == 'bootstrap') {
	$env = require $dir . 'test.php';
} elseif (getenv('APP_ENV') == 'dev') {
	define('DEV', true);
	define('YII_DEBUG', true);
	define('YII_TRACE_LEVEL', 3);
	$env = require $dir . 'development.php';
} elseif (getenv('APP_ENV') == 'staging') {
	define('YII_DEBUG', true);
	define('YII_TRACE_LEVEL', 3);
	$env = require $dir . 'staging.php';
} else {
	$env = require $dir . 'production.php';
	$yii = dirname(__FILE__) . '/../lib/yiisoft/yii/framework/yiilite.php';
}

$config = array_replace_recursive($shared, $env);