<?php
return array(
	'components'=>array(
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'application.lib.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters' => array('*'),
					'levels' => 'error, warning, info, trace',
				),
			),
		),
	),
);