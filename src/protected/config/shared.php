<?php
$domain = ((PHP_SAPI != 'cli') ? substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], '.')) : false);

return array(
	'id'                => 'usic',
	'basePath'          => realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'),
	'name'              => 'USIC Catalog',
	'defaultController' => 'files/index',
	'preload'           => array('log'),

	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'application.lib.sammaye.mongoyii.*',
		'application.lib.sphinx-yii.*',
		'application.lib.mail.*',
		'application.lib.crisu83.yii-extension.behaviors.*',
		'application.lib.pda.pheanstalk.*'
	),

	// application components
	'components'        => array(
		'mongodb'      => array(
			'class'  => 'EMongoClient',
			'server' => 'mongodb://localhost:27017',
			'db'     => 'catalog',
		),
		'user'         => array(
			'allowAutoLogin'  => true,
			'autoUpdateFlash' => false,
			'class'           => 'UsicUser',
			'loginUrl'        => "http://my$domain/?from=http://cat$domain",
		),
		'redisCache'   => array(
			'class'      => 'application.lib.redis.CRedisCache',
			'predisPath' => 'application.lib.redis.Predis',
			'hashKey'    => false,
			'servers'    => array(
				array(
					'database' => 0,
					'host'     => "my" . $domain,
					'port'     => 6379,
				),
			),
		),
		'session'      => array(
			'class'        => 'UsicCCacheHttpSession',
			'cacheID'      => 'redisCache',
			'autoStart'    => true,
			'cookieMode'   => 'allow',
			'cookieParams' =>
				array(
					'path'     => '/',
					'domain'   => $domain,
					'httpOnly' => true,
				)
		),
		'sphinx'       => array(
			'class'             => 'ESphinxConnection',
			'server'            => array('localhost', 9312),
			'connectionTimeout' => 3, // optional, default 0 - no limit
			'queryTimeout'      => 5, // optional, default 0 - no limit
		),
		'mail'         => array(
			'class'     => 'Mailer',
			'transport' => array(
				'class'    => 'Swift_SmtpTransport',
				'host'     => 'MAILHOST',
				'username' => 'MAILUSER',
				'password' => 'MAILPASSWORD',
			),
		),
		'yiinstalk'    => array(
			'class'       => 'Yiinstalk',
			'connections' => array(
				'storage' => array(
					'host' => 'localhost',
					'port' => 11300,
				)
			)
		),
		'urlManager'   => array(
			'showScriptName' => false,
			'caseSensitive'  => false,
			'urlFormat'      => 'path',
			'rules'          => array(
				'files/<category:(books|textbooks|materials|lectures)>' => 'files/index',
				'<action:(login|logout)>'                               => 'site/<action>',
				'<view:(tou|help)>'                                     => 'site/page',
				'files/<id:\w{24}+>'                                    => 'files/view',
				'files/confirm/<id:\w{24}+>'                            => 'files/confirm',
				'/get/<id:\w{24}>/<name>'                               => 'files/download',
				'<controller:\w+>/<action:\w+>/<id:\w{24}>'             => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>'                => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'                         => '<controller>/<action>',
			),
		),
		'request'      => array(
			'enableCsrfValidation' => false,
			'csrfTokenName'        => 'YII_CSRF_TOKEN',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	'params'            => array(
		'adminEmail'        => 'webmaster@usic.at',
		'notificationEmail' => array('MAILUSER' => 'USIC'),
		'maxFileSize'       => 2 * 1024 * 1024, // in bytes. Now 200 Mb

		'preview'           => array(
			'path'       => 'webroot.preview',
			'dimensions' => array(
				array('prefix' => 's', 'width' => 120, 'height' => 160),
				array('prefix' => 'm', 'width' => 450, 'height' => 600),
				array('prefix' => 'l', 'width' => 750, 'height' => 1000),
			)
		),
		// see /views/layouts/meta for details
		'meta'              => array(
			'keywords'           => 'usic,наукма, юізк, студентський інтернет центр, каталог юзік,юзік, вікі юзік, usic, naukma, зареєструватись в юзіку, стажування наукма, могилянка',
			'description'        => 'Каталаго навчальних матеріалів для студентів НаУКМА!',
			'social_description' => 'Усі матеріали у тебе під рукою!',
			'social_title'       => 'Каталог навчальних матеріалів',
			'social_images'      => '/images/logo.png'
		)
	),
);
