<?php
/*
USE WITH CARE

Asumes that each day consists of 60*60*24 seconds 
*/

/* Deletes ALL NOT_APROVED files moderated more then $days ago */

$days = 3;

$seconds = $days*60*60*24;
$timeToDelete =  new MongoDate(time() - $seconds);

$connection = new MongoClient("mongodb://localhost");

$cursor = $connection->storage->files->find(array('status' => 2, 'time_approved' => array('$lt' => $timeToDelete)), array('storage_id'));

$gridFs = $connection->storage->getGridFS('storage');
foreach ($cursor as $doc) {
	$gridFs->remove(array('_id' => new MongoId($doc['storage_id'])));
}

$connection->storage->files->remove(array('status' => 2, 'time_approved' => array('$lt' => $timeToDelete)));

?>